from calc_lib import calculate_sum, random_integer

def test_calc():
    assert calculate_sum(2,3) == 5

def test_random():
    assert random_integer(100) >= 0
    assert random_integer(100) <= 100