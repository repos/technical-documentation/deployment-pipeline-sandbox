from bottle import route, run
from calc_lib import calculate_sum, random_integer

@route('/')
def index():
    random_number = calculate_sum(random_integer(100),random_integer(1000))
    return f"Hello, World! Here's a sum of two random numbers: {random_number}"

run(host='0.0.0.0', port=4040)
