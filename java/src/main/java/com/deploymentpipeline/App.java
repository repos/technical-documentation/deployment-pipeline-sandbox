package com.deploymentpipeline;

import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import com.deploymentpipeline.Calclib;

public class App
{
    public static void main(final String[] args) {
        Undertow server = Undertow.builder()
            .addHttpListener(4040, "0.0.0.0")
                .setHandler(new HttpHandler() {
                    @Override
                    public void handleRequest(final HttpServerExchange exchange) throws Exception {
                        int val = Calclib.calculate_sum(Calclib.generate_random(100),Calclib.generate_random(1000));
                        String response = "Hello World, here's a sum of two numbers: " + val;
                        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/plain");
                        exchange.getResponseSender().send(response);
                    }
                }).build();
        server.start();
    }
}
