package com.deploymentpipeline;

public class Calclib {
    public static int generate_random(int range) {
        return (int)(Math.random() * (range + 1));
    }

    public static int calculate_sum(int o1, int o2) {
        return o1 + o2;
    }
}
