package com.deploymentpipeline;

import org.testng.annotations.*;
import org.testng.Assert;
import com.deploymentpipeline.Calclib;

public class CalclibTest
{
    @Test
    public void shouldGenerateRandomNumbers() {
        int r100 = Calclib.generate_random(100);
        int r1000 = Calclib.generate_random(1000);
        Assert.assertTrue(r100 >= 0 && r100 <= 100);
        Assert.assertTrue(r1000 >= 0 && r1000 <= 1000);
    }

    @Test
    public void shouldCalculateSum() {
        Assert.assertEquals(Calclib.calculate_sum(2,2), 4);
    }
}
