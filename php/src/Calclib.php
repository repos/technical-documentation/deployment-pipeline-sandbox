<?php
declare(strict_types=1);
class Calculator {
    public static function generate_random(int $range): int {
        return rand(0,$range);
    }

    public static function calculate_sum(int $o1, int $o2): int {
        return $o1 + $o2;
    }
}
?>
