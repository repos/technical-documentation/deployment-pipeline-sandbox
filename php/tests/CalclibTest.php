<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;
require __DIR__ . "/../src/Calclib.php";

final class CalclibTest extends TestCase {

    public function testRandom(): void {
        $random100 = Calculator::generate_random(100);
        $random1000 = Calculator::generate_random(1000);
        $this->assertTrue($random100 >= 0 && $random100 <= 100, "Random number generation problem (100)");
        $this->assertTrue($random1000 >= 0 && $random1000 <= 1000, "Random number generation problem (1000)");
    }

    public function testSum(): void {
        $this->assertTrue(Calculator::calculate_sum(2,2) == 4, "Sum calculation problem");
    }
}
