pub fn calculate_sum(a: i32, b: i32) -> i32 {
    return a + b;
}

use rand::Rng;

pub fn random_int(r: i32) -> i32 {
    return rand::thread_rng().gen_range(0..r);
}

#[test]
fn sum_works() {
    assert_eq!(calculate_sum(2,2),4);
}

#[test]
fn random_int_works() {
    assert!(random_int(100) >= 0);
    assert!(random_int(100) <= 100);
}
