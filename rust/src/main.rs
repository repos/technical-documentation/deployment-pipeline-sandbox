#[macro_use] extern crate rocket;
use rocket::Config;
use std::net::{IpAddr, Ipv4Addr};
mod calclib;

#[get("/")]
fn index() -> String {
    let s = calclib::calculate_sum(calclib::random_int(100),calclib::random_int(1000));
    let st: std::string::String = format!("Hello, world! Here's a sum of two random numbers: {s}");
    return st
}

#[rocket::main]
async fn main() -> Result<(), rocket::Error> {
    let config = Config {
        port: 4040,
        address: IpAddr::V4(Ipv4Addr::new(0, 0, 0, 0)),
        ..Config::debug_default()
    };

    let rocket = rocket::custom(&config)
        .mount("/", routes![index])
        .ignite().await?;
    let _result = rocket.launch().await;

    Ok(())
}
