# Deployment pipeline examples

Developed as part of [T352273](https://phabricator.wikimedia.org/T352273), this repository features five small applications in five programming languages: Java, JavaScript/NodeJS, PHP, Python, and Rust. Each application has the same functionality: a web server that responds to user requests by displaying random numbers. Each application also has tests implemented in the given language's popular test framework.

For each application, there is a matching container configuration in `.pipeline/blubber.yaml`, and a matching GitLab CI configuration in `.gitlab-ci.yml`. These configurations might differ between applications to illustrate different features of GitLab, Blubber, and Kokkuri. You can explore these files as practical examples of using the GitLab CI-based deployment pipeline to run and test your applications.

To learn more about using GitLab, Blubber, and Kokkuri see:

* [Deploying services to production](https://www.mediawiki.org/wiki/GitLab/Workflows/Deploying_services_to_production)
* [GitLab documentation on mediawiki.org](https://www.mediawiki.org/wiki/GitLab)
* [Blubber documentation on doc.wikimedia.org](https://doc.wikimedia.org/releng/blubber/)
* [Kokkuri documentation in its repository](https://gitlab.wikimedia.org/repos/releng/kokkuri)

## NodeJS

**Note**: Blubber variants for NodeJS have a different configuration than other language variants. As a result:

* To build the image, run `docker build` using this project's root directory (`docker build (...) .`), instead of a language-specific sub-directory (`docker build (...) ./js`).
* You can omit the `BUILD_CONFIG_CONTEXT` and `BUILD_CONTEXT` variables in `.gitlab-ci.yml`. These variables point to root directory by default and there is no need to override them.

### Running tests

To create a container that runs tests as defined in `js/test.js`:

`docker build -f .pipeline/blubber.yaml --tag test-js --target test-js .`

followed by

`docker run test-js`

### Running a service

To create a container and run the NodeJS application:

`docker build -f .pipeline/blubber.yaml --tag run-js --target run-js .`

followed by

`docker run -p 4040:4040 run-js`

## Python

### Running tests

To create a container and run tests defined in `python/test_calc_lib.py`:

`docker build -f .pipeline/blubber.yaml --tag test-py --target test-py ./python`

followed by

`docker run test-py`

### Running a service

To create a container and run the Python application:

`docker build -f .pipeline/blubber.yaml --tag run-py --target run-py ./python`

followed by

`docker run -p 4040:4040 run-py`

## PHP

### Running tests

To create a container and run tests defined in php/Calclib.php:

`docker build -f .pipeline/blubber.yaml --tag test-php --target test-php ./php`

followed by

`docker run test-php`

### Running a service

To create a container and run the PHP application using a PHP development server:

`docker build -f .pipeline/blubber.yaml --tag run-php --target run-php ./php`

followed by

`docker run -p 4040:4040 run-php`

## Rust

### Running tests

To create a container and run tests defined in `rust/calclib.rs`:

`docker build -f .pipeline/blubber.yaml --tag test-rust --target test-rust ./rust`

followed by

`docker run test-rust`

### Running a service

To create a container and run the Rust application:

`docker build -f .pipeline/blubber.yaml --tag run-rust --target run-rust ./rust`

followed by

`docker run -p 4040:4040 run-rust`
