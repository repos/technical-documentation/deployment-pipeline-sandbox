const calculateSum = (a,b) => {
  return a + b;
};

const randomInt = (range) => {
  return Math.floor(Math.random() * range);
};

module.exports = { calculateSum, randomInt };
