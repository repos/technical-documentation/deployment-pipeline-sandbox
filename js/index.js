const http = require ('http');
const { calculateSum, randomInt } = require ('./calc-lib.js');

const server = http.createServer((request, response) => {
  response.writeHead(200, {'Content-Type': 'text/html'});
  response.write('Hello, World! Here\'s a sum of two random numbers: ' + calculateSum(randomInt(100),randomInt(1000)));
  response.end();
});

server.listen(4040, () => {
  console.log('Server is up on port 4040!');
});
